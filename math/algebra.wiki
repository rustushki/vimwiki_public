= Algebra =

== Interest ==

=== Compound Interest Formula ===

A = P (1 + r/n)^(nt)

A = principal amount + interest
P = principal amount
r = rate
n = number of times interest is compounded per unit 't'
t = time

Example:
{{{

P = $5,000
r = 2%
n = 12 (i.e. 12 months)
t = 1  (i.e. 1 year)

5000 * (1 + 0.02/12)^(12*1) = $5100.92
}}}
