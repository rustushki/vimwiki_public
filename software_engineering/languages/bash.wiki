= Bash =

== Special Variables ==

{{{
$? - return code of last run command
}}}

== IO Redirection ==

=== Redirect stderr to stdout ===

I have to look this up every time. It works like this:

{{{
myCommand 2>&1
}}}

Now myCommand's stderr output will appear in stdout.

=== Redirect stderr and stdout to /dev/null ===

{{{
myCommand > /dev/null 2>&1
}}}

== Functions ==

=== local ===

{{{
function myFunction() {
  local myLocalVariable="something"
}
}}}

Use 'local' to bind a variable to a specific function. Otherwise, the variable
is implicitly part of the global scope.

=== named references / pass by reference ===

{{{
function myFunction() {
  local -n myArgument1=$1
  myArgument=40
}

foo=23
myFunction foo
echo foo
}}}

The output of the above code is 40. The reason why is because foo is passed by
reference. Actually, myArgument1 is declared as a named reference to the value
of the first argument to myFunction.

One thing to be careful about is that the variable passed by reference may not
have the same name in both scopes. Example:

{{{
{{{
function myFunction() {
  local -n myArgument1=$1
}

myArgument1=23
myFunction myArgument1
}}}

This creates a circular named reference.

== Shell Commands ==

=== find ===

Find's options represent a query language--making it substantially different from grep. Different flags represent
operations or operators which define how it will traverse the filesystem, what it will output, or which actions it will
take on each visited file. Here's are some useful arguments:

{{{
 -o      -- OR operator
 -path   -- match the provided path (must be a full relative path)
 -name   -- match the provided name
 -iname  -- match the provided name (ignoring case)
 -type d -- match directories
 -type f -- match files
 -not    -- NOT operator, negate the next condition
 -prune  -- files matching the previous conditions should be excluded from the matches
 -print  -- print the matches
 -exec   -- run a command on the matches
}}}
