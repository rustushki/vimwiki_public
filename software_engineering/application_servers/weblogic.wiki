= WebLogic =

= Starting WebLogic =
{{{
cd %MW_HOME%
cd user_projects\domains\mydomain
.\bin\startWebLogic.cmd
.\bin\stopWebLogic.cmd
}}}

= Web Console =
http://localhost:7001/console

= WLST (CLI) =

== More Info ==
https://docs.oracle.com/cd/E13222_01/wls/docs90/config_scripting/using_WLST.html#1082463

== Running Console ==
{{{
cmd
cd %MW_HOME%
wlserver\server\bin\setWLSEnv.cmd
cd user_projects\domains\mydomain
java weblogic.WLST
}}}

== Connecting to WebLogic Server ==

{{{
connect('weblogic','Weblogic!','t3://localhost:7001')
}}}

== Starting/Stopping Component ==
{{{
startApplication("myapplication")
stopApplication("myapplication")
}}}
