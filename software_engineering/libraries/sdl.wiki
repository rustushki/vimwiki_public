== Getting Display Resolution ==

{{{
int displayIndex = 0;
SDL_DisplayMode displayMode;
SDL_GetCurrentDisplayMode(displayIndex, &displayMode);
}}}

== Fullscreen ==

{{{
SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN);
}}}

Note: The SDL_WINDOW_FULLSCREEN also sets the resolution to the minimum
resolution available.

{{{
SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
}}}

This is a possibly better option. It's actually an 'emulated' fullscreen
(as opposed to a hardware-based) in that it adopts the current video
resolution and the window simply fills all available space. At the moment,
I feel like this is superior to SDL_WINDOW_FULLSCREEN.
	
